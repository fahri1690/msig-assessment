import 'package:flutter/material.dart';
import 'package:libraries/libraries.dart';
import 'package:msig_assessment/main.dart';

import 'flavor/flavor_config.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  FlavorBaseUrlConfig(
    appEnvironment: AppEnvironment.PROD,
  );

  var appConfig = FlavorConfig(
    appEnvironment: AppEnvironment.PROD,
    appName: "App Prod",
    isShowBanner: false,
    child: ModularApp(
      module: AppModule(),
      child: AssessmentApp(),
    ),
  );

  runApp(appConfig);
}
