import 'package:core/core.dart';
import 'package:flutter/material.dart';

enum AppEnvironment { DEV, STAG, PROD }

extension AppEnvironmentText on AppEnvironment {
  String get name {
    switch (this) {
      case AppEnvironment.DEV:
        return "DEV";
      case AppEnvironment.STAG:
        return "STAG";
      case AppEnvironment.PROD:
        return "PROD";
      default:
        return "";
    }
  }

  String get appBaseUrl {
    switch (this) {
      case AppEnvironment.DEV:
        return ApiConstants.ROOT;
      case AppEnvironment.STAG:
        return "";
      case AppEnvironment.PROD:
        return "";
      default:
        return "";
    }
  }
}

class FlavorConfig extends InheritedWidget {
  final AppEnvironment appEnvironment;
  final String appName;
  final bool isShowBanner;
  final Widget child;

  FlavorConfig({
    @required this.appEnvironment,
    @required this.appName,
    @required this.isShowBanner,
    @required this.child,
  });

  static FlavorConfig of(BuildContext context) {
    return context.dependOnInheritedWidgetOfExactType();
  }

  @override
  bool updateShouldNotify(InheritedWidget oldWidget) => false;
}

class FlavorBaseUrlConfig {
  final AppEnvironment appEnvironment;
  static FlavorBaseUrlConfig _instance;

  FlavorBaseUrlConfig._internal(this.appEnvironment);

  static FlavorBaseUrlConfig get instance {
    return _instance;
  }

  factory FlavorBaseUrlConfig({@required AppEnvironment appEnvironment}) {
    _instance ??= FlavorBaseUrlConfig._internal(appEnvironment);
    return _instance;
  }
}
