import 'package:flutter/material.dart';

class FlavorBanner extends StatelessWidget {
  final Widget child;
  final BannerLocation location;
  final String text;
  final TextStyle textStyle;
  final Color color;
  final bool visible;

  const FlavorBanner({
    @required this.text,
    this.location = BannerLocation.topEnd,
    this.textStyle =
    const TextStyle(color: Colors.white, fontWeight: FontWeight.bold),
    this.color = Colors.red,
    this.visible = true,
    @required this.child,
  });

  @override
  Widget build(BuildContext context) {
    if (visible) {
      return Directionality(
        textDirection: TextDirection.ltr,
        child: Banner(
          color: color,
          message: text,
          location: location,
          textStyle: textStyle,
          child: child,
        ),
      );
    }
    return child;
  }
}
