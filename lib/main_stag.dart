import 'package:flutter/material.dart';
import 'package:libraries/libraries.dart';

import 'flavor/flavor_config.dart';
import 'main.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  FlavorBaseUrlConfig(
    appEnvironment: AppEnvironment.STAG,
  );

  var appConfig = FlavorConfig(
    appEnvironment: AppEnvironment.STAG,
    appName: "App Stag",
    isShowBanner: true,
    child: ModularApp(
      module: AppModule(),
      child: AssessmentApp(),
    ),
  );

  runApp(appConfig);
}
