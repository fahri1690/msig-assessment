import 'package:core/utils/api/api.dart';
import 'package:core/utils/routes/main_routes.dart';
import 'package:core/utils/storage/moor_utils.dart';
import 'package:flutter/material.dart';
import 'package:food_list/modules.dart';
import 'package:favorite_list/modules.dart';
import 'package:libraries/libraries.dart';
import 'package:flutter_modular/flutter_modular.dart';

import 'flavor/flavor_banner.dart';
import 'flavor/flavor_config.dart';

class AppModule extends Module {
  @override
  List<Bind> get binds => [
        Bind(
          (_) => Modular.get<DioClient>().dio,
        ),
        Bind(
          (_) => DioClient(),
        ),
        Bind(
          (_) => ApiBaseHelperImpl(
            Modular.get<DioClient>().dio,
          ),
        ),
        Bind((_) => FoodDatabase())
      ];

  Widget get bootstrap => AssessmentApp();

  @override
  final List<ModularRoute> routes = [
    ModuleRoute(MainRoutes.FOOD_LIST, module: FoodListModule()),
    ModuleRoute(MainRoutes.FAVORITES, module: FavoriteFoodModule())
  ];
}

class AssessmentApp extends StatefulWidget {
  @override
  _AssessmentAppState createState() => _AssessmentAppState();
}

class _AssessmentAppState extends State<AssessmentApp> {
  @override
  Widget build(BuildContext context) {
    return FlavorBanner(
      text: FlavorConfig.of(context).appEnvironment.name,
      location: BannerLocation.bottomEnd,
      visible: FlavorConfig.of(context).isShowBanner,
      child: MaterialApp(
        title: FlavorConfig.of(context).appName,
        debugShowCheckedModeBanner: false,
        initialRoute: MainRoutes.FOOD_LIST,
        theme: ThemeData(primaryColor: Colors.white),
      ).modular(),
    );
  }
}
