import 'package:core/utils/storage/moor_utils.dart';
import 'package:favorite_list/data/sources/local/favorite_food_local_service.dart';

abstract class FavoriteFoodServiceRepositories {
  Future<List<FavoriteFood>> getAllFavoriteFood();
  Future deleteFromFavorite(FavoriteFood data);
}

class FavoriteFoodServiceRepositoriesImpl implements FavoriteFoodServiceRepositories{
  final FavoriteFoodsLocalServices local;

  FavoriteFoodServiceRepositoriesImpl(this.local);
  @override
  Future<List<FavoriteFood>> getAllFavoriteFood() async {
    final result = await local.getAllFavoriteFoods();
    return result;
  }

  @override
  Future deleteFromFavorite(FavoriteFood data) => local.deleteFromFavorite(data);
}