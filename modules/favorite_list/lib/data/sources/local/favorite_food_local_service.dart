import 'package:core/utils/storage/moor_utils.dart';

abstract class FavoriteFoodsLocalServices {
  Future<List<FavoriteFood>> getAllFavoriteFoods();

  Future deleteFromFavorite(FavoriteFood data);
}

class FavoriteFoodLocalServicesImpl implements FavoriteFoodsLocalServices {
  final FoodDatabase database;

  FavoriteFoodLocalServicesImpl(this.database);

  @override
  Future<List<FavoriteFood>> getAllFavoriteFoods() async {
    try {
      var result = await database.getAllFavorites();
      return result;
    } catch (e) {
      return null;
    }
  }

  @override
  Future deleteFromFavorite(FavoriteFood data) async {
    if (data != null) {
      final result = await database.deleteFavoriteFood(data);
      return result;
    } else {
      return null;
    }
  }
}
