import 'package:core/utils/storage/moor_utils.dart';
import 'package:favorite_list/data/repositories/favorite_food_service_repositories.dart';

abstract class FavoriteFoodRepositories {
  Future<List<FavoriteFood>> getAllFavoriteFoods();
  Future deleteFromFavorite(FavoriteFood data);
}

class FavoriteFoodRepositoriesImpl implements FavoriteFoodRepositories {

  final FavoriteFoodServiceRepositories repositories;

  FavoriteFoodRepositoriesImpl(this.repositories);

  @override
  Future<List<FavoriteFood>> getAllFavoriteFoods() => repositories.getAllFavoriteFood();

  @override
  Future deleteFromFavorite(FavoriteFood data) => repositories.deleteFromFavorite(data);
}