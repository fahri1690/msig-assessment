import 'package:core/utils/storage/moor_utils.dart';
import 'package:favorite_list/domain/repositories/favorite_food_repositories.dart';

abstract class FavoriteFoodUseCases {
  Future<List<FavoriteFood>> getAllFavoriteFoods();
  Future deleteFromFavorite(FavoriteFood data);
}

class FavoriteFoodUseCaseImpl implements FavoriteFoodUseCases{
  final FavoriteFoodRepositories repositories;

  FavoriteFoodUseCaseImpl(this.repositories);
  @override
  Future<List<FavoriteFood>> getAllFavoriteFoods() => repositories.getAllFavoriteFoods();

  @override
  Future deleteFromFavorite(FavoriteFood data) => repositories.deleteFromFavorite(data);

}