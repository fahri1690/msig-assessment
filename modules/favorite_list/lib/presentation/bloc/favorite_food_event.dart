part of 'favorite_food_bloc.dart';

@immutable
abstract class FavoriteFoodEvent {}

class GetAllFavoriteFoodEvent extends FavoriteFoodEvent {}

class DeleteFavoriteEvent extends FavoriteFoodEvent {
  final FavoriteFood data;

  DeleteFavoriteEvent(this.data);
}
