part of 'favorite_food_bloc.dart';

@immutable
abstract class FavoriteFoodState {}

class FavoriteFoodInitial extends FavoriteFoodState {}

class FavoriteFoodLoadedSuccessState extends FavoriteFoodState {
  final List<FavoriteFood> data;
  FavoriteFoodLoadedSuccessState(this.data);
}

class LoadingState extends FavoriteFoodState {}
