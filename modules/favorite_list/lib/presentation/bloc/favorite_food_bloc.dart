import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:core/utils/storage/moor_utils.dart';
import 'package:favorite_list/domain/usecase/favorite_food_use_case.dart';
import 'package:meta/meta.dart';

part 'favorite_food_event.dart';

part 'favorite_food_state.dart';

class FavoriteFoodBloc extends Bloc<FavoriteFoodEvent, FavoriteFoodState> {
  final FavoriteFoodUseCases usecase;

  FavoriteFoodBloc(this.usecase) : super(FavoriteFoodInitial());

  @override
  Stream<FavoriteFoodState> mapEventToState(
    FavoriteFoodEvent event,
  ) async* {
    if (event is GetAllFavoriteFoodEvent) {
      yield LoadingState();
      final result = await usecase.getAllFavoriteFoods();
      if (result != null) {
        yield FavoriteFoodLoadedSuccessState(result);
      }
    } else if (event is DeleteFavoriteEvent) {
      yield LoadingState();
      await usecase.deleteFromFavorite(event.data);
      final result = await usecase.getAllFavoriteFoods();
      await Future.delayed(Duration(milliseconds: 400));
      yield FavoriteFoodLoadedSuccessState(result);
    }
  }
}
