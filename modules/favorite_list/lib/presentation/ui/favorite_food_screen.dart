import 'package:core/core.dart';
import 'package:favorite_list/presentation/bloc/favorite_food_bloc.dart';
import 'package:flutter/material.dart';
import 'package:libraries/libraries.dart';

class FavoriteFoodScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("My Favorites"),
        centerTitle: true,
      ),
      body: BlocConsumer<FavoriteFoodBloc, FavoriteFoodState>(
        listener: (context, state) {},
        // bloc: Modular.get<FavoriteFoodBloc>()..add(GetAllFavoriteFoodEvent()),
        builder: (context, state) {
          if (state is FavoriteFoodLoadedSuccessState) {
            if (state.data.length <= 0) {
              return Center(
                child: Text(
                  "No Favorite Foods Yet",
                  style: TextStyle(
                      fontSize: 18, letterSpacing: .3, color: Colors.grey),
                ),
              );
            }
            return ListView.separated(
              itemBuilder: (context, index) => Slidable(
                  actionPane: SlidableDrawerActionPane(),
                  secondaryActions: [
                    IconSlideAction(
                      color: Colors.red,
                      icon: Icons.delete_forever_outlined,
                      onTap: () => showDialog(
                          context: context,
                          builder: (context) => AlertDialog(
                                content: Text("Are you sure?"),
                                actions: [
                                  TextButton(
                                      onPressed: () => Navigator.pop(context),
                                      child: Text(
                                        "No",
                                        style: TextStyle(color: Colors.grey),
                                      )),
                                  TextButton(
                                    onPressed: () {
                                      Modular.get<FavoriteFoodBloc>().add(
                                          DeleteFavoriteEvent(
                                              state.data[index]));
                                      Navigator.pop(context);
                                    },
                                    child: Text("Yes"),
                                  )
                                ],
                              )),
                    )
                  ],
                  child: CustomListTile(
                    title: state.data[index].strTitle,
                    subtitle: state.data[index].strDesc,
                    thumbnail: state.data[index].strThumb,
                  )),
              separatorBuilder: (context, index) => Divider(),
              itemCount: state.data.length,
            );
          } else if (state is LoadingState) {
            return LoadingIndicator();
          }
          return Center();
        },
      ),
    );
  }
}
