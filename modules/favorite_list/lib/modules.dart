import 'package:core/utils/storage/moor_utils.dart';
import 'package:favorite_list/data/repositories/favorite_food_service_repositories.dart';
import 'package:favorite_list/data/sources/local/favorite_food_local_service.dart';
import 'package:favorite_list/domain/repositories/favorite_food_repositories.dart';
import 'package:favorite_list/domain/usecase/favorite_food_use_case.dart';
import 'package:favorite_list/presentation/bloc/favorite_food_bloc.dart';
import 'package:favorite_list/presentation/ui/favorite_food_screen.dart';
import 'package:libraries/libraries.dart';

import 'external/routes/favorite_food_routes.dart';

class FavoriteFoodModule extends Module {
  @override
  List<Bind> get binds => [
    Bind((i) => FavoriteFoodLocalServicesImpl(Modular.get<FoodDatabase>())),
    Bind((i) => FavoriteFoodServiceRepositoriesImpl(Modular.get<FavoriteFoodsLocalServices>())),
    Bind((i) => FavoriteFoodRepositoriesImpl(Modular.get<FavoriteFoodServiceRepositories>())),
    Bind((i) => FavoriteFoodUseCaseImpl(Modular.get<FavoriteFoodRepositories>())),
    Bind((i) => FavoriteFoodBloc(Modular.get<FavoriteFoodUseCases>())),
  ];

  @override
  List<ModularRoute> get routes => [
    ChildRoute(FavoriteFoodRoutes.MAIN, child: (_, args) => MultiBlocProvider(providers: [
      BlocProvider(create: (_) => Modular.get<FavoriteFoodBloc>()..add(GetAllFavoriteFoodEvent()))
    ], child: FavoriteFoodScreen()))
  ];
}