import 'package:flutter/material.dart';

class CustomListTile extends StatelessWidget {
  final String thumbnail;
  final String title;
  final String subtitle;

  const CustomListTile({Key key, this.thumbnail, this.title, this.subtitle})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ListTile(
      leading: thumbnail != null
          ? Image.network(
              thumbnail,
              height: 80,
              width: 80,
            )
          : Container(
              height: 80,
              width: 80,
              alignment: Alignment.center,
              child: Text("No Image"),
            ),
      title: Text(title, style: TextStyle(fontWeight: FontWeight.bold),),
      subtitle: Text(
        subtitle,
        overflow: TextOverflow.ellipsis,
        style: TextStyle(letterSpacing: .2, height: 1.6),
      ),
    );
  }
}
