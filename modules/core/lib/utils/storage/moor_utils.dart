import 'package:core/core.dart';
import 'package:libraries/libraries.dart';

part 'moor_utils.g.dart';

class FavoriteFoods extends Table {
  IntColumn get id => integer()();

  TextColumn get strTitle => text()();

  TextColumn get strDesc => text()();

  TextColumn get strThumb => text()();

  @override
  Set<Column> get primaryKey => {id};
}

@UseMoor(tables: [FavoriteFoods])
class FoodDatabase extends _$FoodDatabase {
  FoodDatabase()
      : super(FlutterQueryExecutor.inDatabaseFolder(
            path: DbConstant.path, logStatements: true));

  @override
  int get schemaVersion => DbConstant.version;

  Future<List<FavoriteFood>> getAllFavorites() => select(favoriteFoods).get();

  Stream<List<FavoriteFood>> watchAllFavorites() =>
      select(favoriteFoods).watch();

  Future addFavoriteFood(FavoriteFood food) async {
    return into(favoriteFoods).insert(food).onError((error, stackTrace) {
      return null;
    });
  }

  Future<void> deleteFavoriteFood(FavoriteFood food) {
    return (delete(favoriteFoods)..where((tbl) => tbl.id.equals(food.id))).go();
  }
}
