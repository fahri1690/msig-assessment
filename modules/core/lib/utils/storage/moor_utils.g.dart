// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'moor_utils.dart';

// **************************************************************************
// MoorGenerator
// **************************************************************************

// ignore_for_file: unnecessary_brace_in_string_interps, unnecessary_this
class FavoriteFood extends DataClass implements Insertable<FavoriteFood> {
  final int id;
  final String strTitle;
  final String strDesc;
  final String strThumb;
  FavoriteFood(
      {@required this.id,
      @required this.strTitle,
      @required this.strDesc,
      @required this.strThumb});
  factory FavoriteFood.fromData(Map<String, dynamic> data, GeneratedDatabase db,
      {String prefix}) {
    final effectivePrefix = prefix ?? '';
    final intType = db.typeSystem.forDartType<int>();
    final stringType = db.typeSystem.forDartType<String>();
    return FavoriteFood(
      id: intType.mapFromDatabaseResponse(data['${effectivePrefix}id']),
      strTitle: stringType
          .mapFromDatabaseResponse(data['${effectivePrefix}str_title']),
      strDesc: stringType
          .mapFromDatabaseResponse(data['${effectivePrefix}str_desc']),
      strThumb: stringType
          .mapFromDatabaseResponse(data['${effectivePrefix}str_thumb']),
    );
  }
  @override
  Map<String, Expression> toColumns(bool nullToAbsent) {
    final map = <String, Expression>{};
    if (!nullToAbsent || id != null) {
      map['id'] = Variable<int>(id);
    }
    if (!nullToAbsent || strTitle != null) {
      map['str_title'] = Variable<String>(strTitle);
    }
    if (!nullToAbsent || strDesc != null) {
      map['str_desc'] = Variable<String>(strDesc);
    }
    if (!nullToAbsent || strThumb != null) {
      map['str_thumb'] = Variable<String>(strThumb);
    }
    return map;
  }

  FavoriteFoodsCompanion toCompanion(bool nullToAbsent) {
    return FavoriteFoodsCompanion(
      id: id == null && nullToAbsent ? const Value.absent() : Value(id),
      strTitle: strTitle == null && nullToAbsent
          ? const Value.absent()
          : Value(strTitle),
      strDesc: strDesc == null && nullToAbsent
          ? const Value.absent()
          : Value(strDesc),
      strThumb: strThumb == null && nullToAbsent
          ? const Value.absent()
          : Value(strThumb),
    );
  }

  factory FavoriteFood.fromJson(Map<String, dynamic> json,
      {ValueSerializer serializer}) {
    serializer ??= moorRuntimeOptions.defaultSerializer;
    return FavoriteFood(
      id: serializer.fromJson<int>(json['id']),
      strTitle: serializer.fromJson<String>(json['strTitle']),
      strDesc: serializer.fromJson<String>(json['strDesc']),
      strThumb: serializer.fromJson<String>(json['strThumb']),
    );
  }
  @override
  Map<String, dynamic> toJson({ValueSerializer serializer}) {
    serializer ??= moorRuntimeOptions.defaultSerializer;
    return <String, dynamic>{
      'id': serializer.toJson<int>(id),
      'strTitle': serializer.toJson<String>(strTitle),
      'strDesc': serializer.toJson<String>(strDesc),
      'strThumb': serializer.toJson<String>(strThumb),
    };
  }

  FavoriteFood copyWith(
          {int id, String strTitle, String strDesc, String strThumb}) =>
      FavoriteFood(
        id: id ?? this.id,
        strTitle: strTitle ?? this.strTitle,
        strDesc: strDesc ?? this.strDesc,
        strThumb: strThumb ?? this.strThumb,
      );
  @override
  String toString() {
    return (StringBuffer('FavoriteFood(')
          ..write('id: $id, ')
          ..write('strTitle: $strTitle, ')
          ..write('strDesc: $strDesc, ')
          ..write('strThumb: $strThumb')
          ..write(')'))
        .toString();
  }

  @override
  int get hashCode => $mrjf($mrjc(id.hashCode,
      $mrjc(strTitle.hashCode, $mrjc(strDesc.hashCode, strThumb.hashCode))));
  @override
  bool operator ==(dynamic other) =>
      identical(this, other) ||
      (other is FavoriteFood &&
          other.id == this.id &&
          other.strTitle == this.strTitle &&
          other.strDesc == this.strDesc &&
          other.strThumb == this.strThumb);
}

class FavoriteFoodsCompanion extends UpdateCompanion<FavoriteFood> {
  final Value<int> id;
  final Value<String> strTitle;
  final Value<String> strDesc;
  final Value<String> strThumb;
  const FavoriteFoodsCompanion({
    this.id = const Value.absent(),
    this.strTitle = const Value.absent(),
    this.strDesc = const Value.absent(),
    this.strThumb = const Value.absent(),
  });
  FavoriteFoodsCompanion.insert({
    this.id = const Value.absent(),
    @required String strTitle,
    @required String strDesc,
    @required String strThumb,
  })  : strTitle = Value(strTitle),
        strDesc = Value(strDesc),
        strThumb = Value(strThumb);
  static Insertable<FavoriteFood> custom({
    Expression<int> id,
    Expression<String> strTitle,
    Expression<String> strDesc,
    Expression<String> strThumb,
  }) {
    return RawValuesInsertable({
      if (id != null) 'id': id,
      if (strTitle != null) 'str_title': strTitle,
      if (strDesc != null) 'str_desc': strDesc,
      if (strThumb != null) 'str_thumb': strThumb,
    });
  }

  FavoriteFoodsCompanion copyWith(
      {Value<int> id,
      Value<String> strTitle,
      Value<String> strDesc,
      Value<String> strThumb}) {
    return FavoriteFoodsCompanion(
      id: id ?? this.id,
      strTitle: strTitle ?? this.strTitle,
      strDesc: strDesc ?? this.strDesc,
      strThumb: strThumb ?? this.strThumb,
    );
  }

  @override
  Map<String, Expression> toColumns(bool nullToAbsent) {
    final map = <String, Expression>{};
    if (id.present) {
      map['id'] = Variable<int>(id.value);
    }
    if (strTitle.present) {
      map['str_title'] = Variable<String>(strTitle.value);
    }
    if (strDesc.present) {
      map['str_desc'] = Variable<String>(strDesc.value);
    }
    if (strThumb.present) {
      map['str_thumb'] = Variable<String>(strThumb.value);
    }
    return map;
  }

  @override
  String toString() {
    return (StringBuffer('FavoriteFoodsCompanion(')
          ..write('id: $id, ')
          ..write('strTitle: $strTitle, ')
          ..write('strDesc: $strDesc, ')
          ..write('strThumb: $strThumb')
          ..write(')'))
        .toString();
  }
}

class $FavoriteFoodsTable extends FavoriteFoods
    with TableInfo<$FavoriteFoodsTable, FavoriteFood> {
  final GeneratedDatabase _db;
  final String _alias;
  $FavoriteFoodsTable(this._db, [this._alias]);
  final VerificationMeta _idMeta = const VerificationMeta('id');
  GeneratedIntColumn _id;
  @override
  GeneratedIntColumn get id => _id ??= _constructId();
  GeneratedIntColumn _constructId() {
    return GeneratedIntColumn(
      'id',
      $tableName,
      false,
    );
  }

  final VerificationMeta _strTitleMeta = const VerificationMeta('strTitle');
  GeneratedTextColumn _strTitle;
  @override
  GeneratedTextColumn get strTitle => _strTitle ??= _constructStrTitle();
  GeneratedTextColumn _constructStrTitle() {
    return GeneratedTextColumn(
      'str_title',
      $tableName,
      false,
    );
  }

  final VerificationMeta _strDescMeta = const VerificationMeta('strDesc');
  GeneratedTextColumn _strDesc;
  @override
  GeneratedTextColumn get strDesc => _strDesc ??= _constructStrDesc();
  GeneratedTextColumn _constructStrDesc() {
    return GeneratedTextColumn(
      'str_desc',
      $tableName,
      false,
    );
  }

  final VerificationMeta _strThumbMeta = const VerificationMeta('strThumb');
  GeneratedTextColumn _strThumb;
  @override
  GeneratedTextColumn get strThumb => _strThumb ??= _constructStrThumb();
  GeneratedTextColumn _constructStrThumb() {
    return GeneratedTextColumn(
      'str_thumb',
      $tableName,
      false,
    );
  }

  @override
  List<GeneratedColumn> get $columns => [id, strTitle, strDesc, strThumb];
  @override
  $FavoriteFoodsTable get asDslTable => this;
  @override
  String get $tableName => _alias ?? 'favorite_foods';
  @override
  final String actualTableName = 'favorite_foods';
  @override
  VerificationContext validateIntegrity(Insertable<FavoriteFood> instance,
      {bool isInserting = false}) {
    final context = VerificationContext();
    final data = instance.toColumns(true);
    if (data.containsKey('id')) {
      context.handle(_idMeta, id.isAcceptableOrUnknown(data['id'], _idMeta));
    }
    if (data.containsKey('str_title')) {
      context.handle(_strTitleMeta,
          strTitle.isAcceptableOrUnknown(data['str_title'], _strTitleMeta));
    } else if (isInserting) {
      context.missing(_strTitleMeta);
    }
    if (data.containsKey('str_desc')) {
      context.handle(_strDescMeta,
          strDesc.isAcceptableOrUnknown(data['str_desc'], _strDescMeta));
    } else if (isInserting) {
      context.missing(_strDescMeta);
    }
    if (data.containsKey('str_thumb')) {
      context.handle(_strThumbMeta,
          strThumb.isAcceptableOrUnknown(data['str_thumb'], _strThumbMeta));
    } else if (isInserting) {
      context.missing(_strThumbMeta);
    }
    return context;
  }

  @override
  Set<GeneratedColumn> get $primaryKey => {id};
  @override
  FavoriteFood map(Map<String, dynamic> data, {String tablePrefix}) {
    final effectivePrefix = tablePrefix != null ? '$tablePrefix.' : null;
    return FavoriteFood.fromData(data, _db, prefix: effectivePrefix);
  }

  @override
  $FavoriteFoodsTable createAlias(String alias) {
    return $FavoriteFoodsTable(_db, alias);
  }
}

abstract class _$FoodDatabase extends GeneratedDatabase {
  _$FoodDatabase(QueryExecutor e) : super(SqlTypeSystem.defaultInstance, e);
  $FavoriteFoodsTable _favoriteFoods;
  $FavoriteFoodsTable get favoriteFoods =>
      _favoriteFoods ??= $FavoriteFoodsTable(this);
  @override
  Iterable<TableInfo> get allTables => allSchemaEntities.whereType<TableInfo>();
  @override
  List<DatabaseSchemaEntity> get allSchemaEntities => [favoriteFoods];
}
