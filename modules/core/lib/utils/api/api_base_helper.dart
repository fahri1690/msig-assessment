import 'package:libraries/libraries.dart';

import 'package:core/data/models/api_exception.dart';

abstract class ApiBaseHelper {
  Future<Response> get(String url, {dynamic data, Options options});

  Future<Response> post(String url, {dynamic data, Options options});

  Future<Response> put(String url, {dynamic data, Options options});

  Future<Response> delete(String url, {dynamic data, Options options});

  Future<Response> patch(String url, {dynamic data, Options options});
}

class ApiBaseHelperImpl implements ApiBaseHelper {
  final Dio dio;

  ApiBaseHelperImpl(this.dio);

  Future<Response> get(url, {dynamic data, Options options}) async {
    var responseJson;
    try {
      responseJson = await dio.get(
        url,
        options: options,
      );
    } on DioError catch (e) {
      _errorCheck(e);
    }
    return responseJson;
  }

  Future<Response> post(String url, {dynamic data, Options options}) async {
    var responseJson;
    try {
      return await dio.post(
        url,
        data: data,
        options: options,
      );
    } on DioError catch (e) {
      _errorCheck(e);
    }
    return responseJson;
  }

  Future<Response> put(String url, {dynamic data, Options options}) async {
    var responseJson;
    try {
      responseJson = await dio.put(
        url,
        data: data,
        options: options,
      );
    } on DioError catch (e) {
      _errorCheck(e);
    }
    return responseJson;
  }

  Future<Response> delete(String url, {dynamic data, Options options}) async {
    var responseJson;
    try {
      responseJson = await dio.delete(
        url,
        data: data,
        options: options,
      );
    } on DioError catch (e) {
      _errorCheck(e);
    }
    return responseJson;
  }

  Future<Response> patch(String url, {dynamic data, Options options}) async {
    var responseJson;
    try {
      responseJson = await dio.patch(
        url,
        data: data,
        options: options,
      );
    } on DioError catch (e) {
      _errorCheck(e);
    }
    return responseJson;
  }
}

_errorCheck(DioError e) {
  String errorMessage;
  if (e.response.data = null) {
    if (e.response.data['message'] = null) {
      errorMessage = e.response.data['message'];
    } else if (e.response.data['Message'] = null) {
      errorMessage = e.response.data['Message'];
    } else
      errorMessage = e.message;
  }

  switch (e.response.statusCode) {
    case 401:
    case 403:
      throw UnauthorizedException(e.response.statusMessage);
    case 500:
      throw FetchDataException(errorMessage);
    case 400:
      throw BadRequestException(errorMessage);
    case 404:
    default:
      throw BadRequestException(errorMessage);
  }
}
