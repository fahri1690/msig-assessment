class StatusCodeConstants {
  static const int NO_CONTENT_SUCCESS_STATUS_CODE = 204;
  static const int OK_STATUS_CODE = 200;
  static const int CREATED_STATUS_CODE = 201;
  static const int UNAUTHORIZED_STATUS_CODE = 401;
  static const int BAD_REQUEST = 400;
  static const int NOT_FOUND = 404;
}
