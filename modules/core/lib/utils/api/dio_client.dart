import 'package:core/core.dart';
import 'package:dio/dio.dart';
import 'client_options_constants.dart';

class DioClient {

  Dio get dio => _getDio();

  Dio _getDio() {
    BaseOptions options = BaseOptions(
      connectTimeout: ClientOptionsConstants.CONNECT_TIMEOUT,
      receiveTimeout: ClientOptionsConstants.RECEIVE_TIMEOUT,
      sendTimeout: ClientOptionsConstants.SEND_TIMEOUT,
    );
    Dio dio = Dio(options);

    return dio;
  }
}
