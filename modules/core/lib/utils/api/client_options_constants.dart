class ClientOptionsConstants {
  static const int CONNECT_TIMEOUT = 30000;
  static const int RECEIVE_TIMEOUT = 30000;
  static const int SEND_TIMEOUT = 30000;
}
