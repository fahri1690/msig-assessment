class ApiConstants {
  static const String ROOT = 'https://www.themealdb.com/';
  static const String VERSION = 'api/json/v1/1';
}