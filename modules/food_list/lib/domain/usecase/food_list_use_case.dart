import 'package:core/utils/storage/moor_utils.dart';
import 'package:food_list/domain/repositories/food_list_repositories.dart';
import 'package:libraries/libraries.dart';

abstract class FoodListUseCase {
  Future<Response> getFoodListCategories();

  Future addToFavorite(FavoriteFood data);
}

class FoodListUseCaseImpl implements FoodListUseCase {
  final FoodListRepositories repositories;

  const FoodListUseCaseImpl(this.repositories);

  @override
  Future<Response> getFoodListCategories() =>
      repositories.getFoodListCategories();

  @override
  Future addToFavorite(FavoriteFood data) async {
    final res = await repositories.addToFavorite(data);

    return res;
  }
}
