import 'package:core/utils/storage/moor_utils.dart';

import 'package:food_list/data/repositories/food_service_repositories.dart';
import 'package:libraries/libraries.dart';

abstract class FoodListRepositories {
  Future<Response> getFoodListCategories();

  Future addToFavorite(FavoriteFood data);
}

class FoodListRepositoriesImpl implements FoodListRepositories {
  final FoodServiceRepositories repositories;

  const FoodListRepositoriesImpl(this.repositories);

  @override
  Future<Response> getFoodListCategories() => repositories.getFoodCategories();

  @override
  Future addToFavorite(FavoriteFood data) async {
    final res = await repositories.addToFavorite(data);

    return res;
  }
}
