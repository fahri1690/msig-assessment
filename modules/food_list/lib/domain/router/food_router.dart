import 'package:core/utils/routes/main_routes.dart';
import 'package:libraries/libraries.dart';

abstract class FoodListRouter {
  goToFavoritesFoodList();
}

class FoodListRouterImpl implements FoodListRouter {
  @override
  goToFavoritesFoodList() => Modular.to.pushNamed(MainRoutes.FAVORITES);
}