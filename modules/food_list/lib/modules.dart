import 'package:core/core.dart';
import 'package:core/utils/storage/moor_utils.dart';
import 'package:food_list/data/repositories/food_service_repositories.dart';
import 'package:food_list/data/sources/local/food_list_local_services.dart';
import 'package:food_list/data/sources/remote/food_list_remote_services.dart';
import 'package:food_list/domain/repositories/food_list_repositories.dart';
import 'package:food_list/domain/router/food_router.dart';
import 'package:food_list/domain/usecase/food_list_use_case.dart';
import 'package:food_list/external/routes/food_list_routes.dart';
import 'package:food_list/presentation/bloc/food_list_bloc.dart';
import 'package:libraries/libraries.dart';

import 'presentation/ui/food_list_screen.dart';

class FoodListModule extends Module {

  @override
  List<Bind> get binds => [
    Bind((_) => FoodListRouterImpl()),
    Bind((i) => FoodListRemoteServiceImpl(helper: Modular.get<ApiBaseHelper>())),
    Bind((i) => FoodListLocalServicesImpl(Modular.get<FoodDatabase>())),
    Bind((i) => FoodServiceRepositoriesImpl(service: Modular.get<FoodListRemoteService>(), local: Modular.get<FoodListLocalServices>())),
    Bind((i) => FoodListRepositoriesImpl(Modular.get<FoodServiceRepositories>())),
    Bind((i) => FoodListUseCaseImpl(Modular.get<FoodListRepositories>())),
    Bind((i) => FoodListBloc(Modular.get<FoodListUseCase>())),
  ];

  @override
  List<ModularRoute> get routes => [
    ChildRoute(FoodListRoutes.main, child: (_,args) => MultiBlocProvider(providers: [
      BlocProvider(create: (_) => Modular.get<FoodListBloc>())
    ], child: FoodListScreen()))
  ];
}