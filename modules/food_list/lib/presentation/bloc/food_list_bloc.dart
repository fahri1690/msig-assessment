import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:core/utils/storage/moor_utils.dart';
import 'package:food_list/data/models/categories.dart';
import 'package:food_list/data/models/food_categories_model.dart';
import 'package:food_list/domain/usecase/food_list_use_case.dart';
import 'package:libraries/libraries.dart';
import 'package:meta/meta.dart';

part 'food_list_event.dart';

part 'food_list_state.dart';

class FoodListBloc extends Bloc<FoodListEvent, FoodListState> {
  final FoodListUseCase useCase;

  FoodListBloc(this.useCase) : super(FoodListInitial());

  @override
  Stream<FoodListState> mapEventToState(
    FoodListEvent event,
  ) async* {
    if (event is GetFoodCategoriesEvent) {
      yield* _getFoodList();
    }

    if (event is AddToFavoriteEvent) {
      FavoriteFood result = FavoriteFood(
          id: int.tryParse(event.data.idCategory),
          strTitle: event.data.strCategory,
          strDesc: event.data.strCategoryDescription,
          strThumb: event.data.strCategoryThumb);

      final res = await useCase.addToFavorite(result);

      if (res == null) {
        yield* _getFoodList(
            message: "This food already added in your favorites!");
      } else {
        yield* _getFoodList(message: "Success add to favorites");
      }
    }
  }

  Stream<FoodListState> _getFoodList({String message}) async* {
    final result = await useCase.getFoodListCategories();

    if (result != null) {
      final data = FoodCategoriesModel.fromJson(result.data);
      yield FoodListLoadedSuccessState(
          model: data.categories, message: message);
    } else {
      yield FoodListLoadedErrorState("Error");
    }
  }
}
