part of 'food_list_bloc.dart';

@immutable
abstract class FoodListState {}

class FoodListInitial extends FoodListState {}

class FoodListLoadedSuccessState extends FoodListState {
  final String message;
  final List<Categories> model;

  FoodListLoadedSuccessState({this.message, @required this.model});
}

class FoodListLoadedErrorState extends FoodListState {
  final String message;

  FoodListLoadedErrorState(this.message);
}
