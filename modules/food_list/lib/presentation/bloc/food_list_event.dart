part of 'food_list_bloc.dart';

@immutable
abstract class FoodListEvent {}

class GetFoodCategoriesEvent extends FoodListEvent {}

class AddToFavoriteEvent extends FoodListEvent {
  final Categories data;

  AddToFavoriteEvent(this.data);
}
