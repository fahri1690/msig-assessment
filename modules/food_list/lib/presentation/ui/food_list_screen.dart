import 'package:core/core.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:food_list/domain/router/food_router.dart';
import 'package:food_list/presentation/bloc/food_list_bloc.dart';
import 'package:libraries/libraries.dart' as p;

class FoodListScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        centerTitle: true,
        title: Text("Food List"),
        actions: [
          IconButton(
            icon: Icon(Icons.favorite_border),
            onPressed: () =>
                p.Modular.get<FoodListRouter>().goToFavoritesFoodList(),
          )
        ],
      ),
      body: p.BlocConsumer<FoodListBloc, FoodListState>(
        bloc: p.Modular.get<FoodListBloc>()..add(GetFoodCategoriesEvent()),
        listener: (context, state) {
          if (state is FoodListLoadedSuccessState) {
            return _showSnackbar(state.message, context);
          }
        },
        builder: (context, state) {
          if (state is FoodListLoadedSuccessState) {
            return ListView.separated(
              separatorBuilder: (context, index) => Divider(
                thickness: .1,
              ),
              padding: EdgeInsets.all(16.0),
              itemCount: state.model.length,
              itemBuilder: (context, index) => p.Slidable(
                actionPane: p.SlidableDrawerActionPane(),
                secondaryActions: [
                  p.IconSlideAction(
                    color: Colors.green,
                    icon: Icons.favorite_border_outlined,
                    onTap: () => p.Modular.get<FoodListBloc>()
                        .add(AddToFavoriteEvent(state.model[index])),
                  )
                ],
                child: GestureDetector(
                    onTap: () => showDialog(
                        context: context,
                        builder: (context) => AlertDialog(
                              contentPadding: EdgeInsets.zero,
                              content: Column(
                                mainAxisSize: MainAxisSize.min,
                                children: <Widget>[
                                  Stack(
                                    alignment: Alignment.topLeft,
                                    children: [
                                      Image.network(
                                          state.model[index].strCategoryThumb),
                                      IconButton(
                                          icon: Icon(Icons.clear),
                                          onPressed: () =>
                                              Navigator.pop(context)),
                                    ],
                                  ),
                                  Expanded(
                                    child: ListView(
                                      shrinkWrap: true,
                                      padding: EdgeInsets.all(16.0),
                                      children: <Widget>[
                                        Text(state.model[index]
                                            .strCategoryDescription)
                                      ],
                                    ),
                                  )
                                ],
                              ),
                              actionsPadding: EdgeInsets.zero,
                              actions: [
                                ElevatedButton(
                                  style: ButtonStyle(),
                                  child: Row(
                                    mainAxisSize: MainAxisSize.max,
                                    children: [
                                      Icon(Icons.favorite_border_outlined),
                                      SizedBox(
                                        width: 14,
                                      ),
                                      Text(
                                        "Add to favorite",
                                        style: TextStyle(fontSize: 16),
                                      )
                                    ],
                                  ),
                                  onPressed: () {
                                    p.Modular.get<FoodListBloc>().add(
                                      AddToFavoriteEvent(state.model[index]),
                                    );
                                    Navigator.pop(context);
                                  },
                                ),
                              ],
                            )),
                    child: CustomListTile(
                      thumbnail: state.model[index].strCategoryThumb,
                      title: state.model[index].strCategory,
                      subtitle: state.model[index].strCategoryDescription,
                    )),
              ),
            );
          } else {
            return LoadingIndicator();
          }
        },
      ),
    );
  }

  ScaffoldFeatureController<SnackBar, SnackBarClosedReason> _showSnackbar(
      String message, BuildContext context) {
    if (message != null) {
      final snackBar = SnackBar(
        content: Text(message),
        duration: Duration(seconds: 2),
      );
      return ScaffoldMessenger.of(context).showSnackBar(snackBar);
    } else {
      return null;
    }
  }
}
