import 'package:core/core.dart';

class FoodListConstant {
  static const String CATEGORIES = '${ApiConstants.ROOT}/${ApiConstants.VERSION}/categories.php';
}