import 'package:core/utils/storage/moor_utils.dart';
import 'package:flutter/foundation.dart';
import 'package:food_list/data/sources/local/food_list_local_services.dart';
import 'package:food_list/data/sources/remote/food_list_remote_services.dart';
import 'package:libraries/libraries.dart';

abstract class FoodServiceRepositories {
  Future<Response> getFoodCategories();

  Future addToFavorite(FavoriteFood data);
}

class FoodServiceRepositoriesImpl implements FoodServiceRepositories {
  final FoodListRemoteService service;
  final FoodListLocalServices local;

  const FoodServiceRepositoriesImpl(
      {@required this.service, @required this.local});

  @override
  Future<Response> getFoodCategories() async {
    try {
      var result = await service.getFoodCategories();
      return result;
    } catch (e) {
      return null;
    }
  }

  @override
  Future addToFavorite(FavoriteFood data) async {
    final res = await local.addToFavorite(data);
    return res;
  }
}
