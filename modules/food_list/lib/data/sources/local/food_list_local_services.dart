import 'package:core/utils/storage/moor_utils.dart';

abstract class FoodListLocalServices {
  Future addToFavorite(FavoriteFood data);
}

class FoodListLocalServicesImpl implements FoodListLocalServices {
  final FoodDatabase database;

  FoodListLocalServicesImpl(this.database);

  @override
  Future addToFavorite(FavoriteFood data) async {
    final res = await database.addFavoriteFood(data);
    if (res != null) {
      return res;
    } else {
      return null;
    }
  }
}
