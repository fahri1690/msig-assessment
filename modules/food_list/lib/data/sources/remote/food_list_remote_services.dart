import 'package:core/core.dart';
import 'package:flutter/foundation.dart';
import 'package:food_list/external/constant/food_list_constant.dart';
import 'package:libraries/libraries.dart';

abstract class FoodListRemoteService {
  Future<Response> getFoodCategories();
}

class FoodListRemoteServiceImpl implements FoodListRemoteService {
  final ApiBaseHelper helper;

  const FoodListRemoteServiceImpl({@required this.helper});

  @override
  Future<Response> getFoodCategories() async {
    final Response result = await helper.get(FoodListConstant.CATEGORIES);
    if(result != null) {
      return result;
    }else {
      return null;
    }
  }
}
