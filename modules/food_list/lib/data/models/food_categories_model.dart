import 'categories.dart';

class FoodCategoriesModel {
  List<Categories> categories;

  FoodCategoriesModel({
      this.categories});

  FoodCategoriesModel.fromJson(dynamic json) {
    if (json["categories"] != null) {
      categories = [];
      json["categories"].forEach((v) {
        categories.add(Categories.fromJson(v));
      });
    }
  }
}