class Categories {
  String idCategory;
  String strCategory;
  String strCategoryThumb;
  String strCategoryDescription;

  Categories({
      this.idCategory, 
      this.strCategory, 
      this.strCategoryThumb, 
      this.strCategoryDescription});

  Categories.fromJson(dynamic json) {
    idCategory = json["idCategory"];
    strCategory = json["strCategory"];
    strCategoryThumb = json["strCategoryThumb"];
    strCategoryDescription = json["strCategoryDescription"];
  }

  Map<String, dynamic> toJson() {
    var map = <String, dynamic>{};
    map["idCategory"] = idCategory;
    map["strCategory"] = strCategory;
    map["strCategoryThumb"] = strCategoryThumb;
    map["strCategoryDescription"] = strCategoryDescription;
    return map;
  }

}